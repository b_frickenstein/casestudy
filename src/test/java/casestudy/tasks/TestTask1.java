package casestudy.tasks;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import task1.Task1;

public class TestTask1 {
	Task1 task;	
	List<String> list1 = new ArrayList<>();
	List<String> list2 = new ArrayList<>();
	
	@Before
	public void testData() {
		this.task = new Task1();
		
		this.list1.add("Name1");
		this.list1.add("Name2");
		this.list1.add("Name3");
		
		this.list2.add("Name1");
		this.list2.add("Name3");
		this.list2.add("Name4");
	}
	
	@Test
	public void testinBoth() {
		assertEquals("Name1", this.task.inBoth(list1, list2).get(0));
		assertEquals("Name3", this.task.inBoth(list1, list2).get(1));
	}
	
	@Test
	public void testOnlyInList() {
		assertEquals("Name2",this.task.onlyInList(this.list1, this.list2).get(0));
		assertEquals("Name4",this.task.onlyInList(this.list2, this.list1).get(0));
	}
	
	@Test
	public void testInList() {
		assertEquals(true, this.task.inList(this.list1, "Name1"));
		assertEquals(true, this.task.inList(this.list1, "Name2"));
		assertEquals(false, this.task.inList(this.list1, "Name5"));
		assertEquals(false, this.task.inList(this.list1, "Name6"));
	}
	
}

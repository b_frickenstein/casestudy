package casestudy.tasks;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import task2.Task2;

public class TestTask2 {
  	Task2 task;
  	
  	@Before
  	public void testData() {
  		this.task = new Task2();
  		this.task.loadProducts();
  	}
  
  	@Test
  	public void testGetMostExpensiveProduct() {
  		assertEquals("product1", this.task.getMostExpensiveProduct());
  	}
  	
  	@Test
  	public void testGetCheapestProduct() {
  		assertEquals("product3", this.task.getCheapestProduct());
  	}
  	
  	@Test
  	public void testGetMostPopularProduct() {
  		assertEquals("product2", this.task.getMostPopularProduct());
  	}
  	
  	@Test
  	public void testGetProductsFrom() {
  		assertEquals("product1", this.task.getProductsFrom("DE").get(0));
  		assertEquals("product3", this.task.getProductsFrom("ES").get(0));
  	}
  	
  	@Test
  	public void testIsFragileProducts() {
  		assertEquals(true, this.task.isFragileProducts());
  	}
	
}

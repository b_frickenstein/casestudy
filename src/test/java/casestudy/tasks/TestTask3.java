package casestudy.tasks;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import task3.Flear;
import task3.Task3;

public class TestTask3 {
	ArrayList<Flear> flears = new ArrayList<>();
	
	@Test
	public void testGetOptimalValue() {
		assertEquals(4, Task3.getOptimalValue(1, this.flears));
		assertEquals(6, Task3.getOptimalValue(3, this.flears));
		assertEquals(8, Task3.getOptimalValue(4, this.flears));
		assertEquals(20,Task3.getOptimalValue(16, this.flears));
	}
	
	@Before
	public void generateTestData() {
		this.flears.add(new Flear("Floh 1", 6, 5));
		this.flears.add(new Flear("Floh 2", 4, 5));
		this.flears.add(new Flear("Floh 3", 2, 2));
		this.flears.add(new Flear("Floh 4", 3, 4));
		this.flears.add(new Flear("Floh 4", 1, 4));
	}
	
}

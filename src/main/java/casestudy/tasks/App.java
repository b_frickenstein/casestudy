package casestudy.tasks;

import java.util.ArrayList;
import java.util.List;

import task1.Task1;
import task2.Task2;
import task3.Flear;
import task3.Task3;

public class App {
	static List<Flear> flears = new ArrayList<Flear>();
	
	public App() {
		System.out.println("test");
	}
    public static void main( String[] args )
    {
    	System.out.println("Task 1 ==================>");
		Task1 ts1 = new Task1();
		ts1.run();
		System.out.println(ts1.getJson());
		System.out.println();
		
		System.out.println("Task 2 ==================>");
		Task2 ts2 = new Task2();
		ts2.loadProducts();
		System.out.println(ts2.exportJson());
		System.out.println();
		
		System.out.println("Task 3 ==================>");
		App.generateFlearData();
		System.out.println("Best Rating: " + Task3.getOptimalValue(30, App.flears));
    }
    
    public static void generateFlearData() {
		flears.add(new Flear("Floh 1", 4, 7));
		flears.add(new Flear("Floh 2", 9, 10));
		flears.add(new Flear("Floh 3", 2, 5));
		flears.add(new Flear("Floh 4", 9, 10));
		flears.add(new Flear("Floh 5", 11, 1));
		flears.add(new Flear("Floh 6", 20, 7));
		flears.add(new Flear("Floh 7", 4, 3));
		flears.add(new Flear("Floh 8", 4, 5));
		flears.add(new Flear("Floh 9", 6, 2));
		flears.add(new Flear("Floh 10", 3, 1));
		flears.add(new Flear("Floh 11", 2, 7));
		flears.add(new Flear("Floh 12", 1, 12));
		flears.add(new Flear("Floh 13", 4, 7));
		flears.add(new Flear("Floh 14", 9, 10));
		flears.add(new Flear("Floh 15", 2, 5));
		flears.add(new Flear("Floh 16", 2, 5));
		flears.add(new Flear("Floh 17", 1, 6));
		flears.add(new Flear("Floh 18", 6, 2));
		flears.add(new Flear("Floh 19", 6, 2));
	}
}

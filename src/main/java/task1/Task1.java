package task1;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import FileHandler.FileHandler;

public class Task1 {

	private JsonOutput jsonOutput;
	private List<String> list1;
	private List<String> list2;
	
	public Task1() {}
	
	public void run() {
		this.jsonOutput = new JsonOutput();
		this.loadTextfiles();
		this.init();
	}
	
	private void loadTextfiles() {
		this.list1 = FileHandler.linesToList("task1/List1.txt");
		this.list2 = FileHandler.linesToList("task1/List2.txt");
	}
	
	public String getJson() {
		// Create "Pretty" Json String
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this.jsonOutput);
	}
	
	private void init() {
		this.jsonOutput.onlyInList1 = this.onlyInList(this.list1, this.list2);
		this.jsonOutput.onlyInList2 = this.onlyInList(this.list2, this.list1);
		this.jsonOutput.inBothLists = this.inBoth(this.list1, this.list2);
	}
	
	public List<String> inBoth(List<String> list1, List<String> list2) {
		ArrayList<String> output = new ArrayList<String>();
		
		for(String item : list1){
			if (inList(list2, item)) {
				// Add name to Output
				output.add(item);
			}
		}
		return output;
	}
	
	public List<String> onlyInList(List<String> list1, List<String> list2) {
		List<String> output = new ArrayList<String>();
		
		for(String item : list1){			
			// Check if name not in list2
			if (!this.inList(list2, item)) {
				// Add name to output
				output.add(item);
			}
		}
		return output;
	}
	
	public boolean inList(List<String> list, String name) {
		for(String item : list){	
			// Check if name in List
			if (item.equals(name)) {
				return true;
			}
		}
		// No Result
		return false;
	}
	
}

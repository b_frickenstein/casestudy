package FileHandler;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

public class FileHandler {
	public static List<String> linesToList(String filepath) {
		URL url = Resources.getResource(filepath);
		
		try {
			// Load from Resource
			List<String> lines = Resources.readLines(url, Charsets.UTF_8);
			
			// Return Lines
			return lines;
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// No Result
		return null;
	}
	
	public static String getContent(String filepath) {
		URL url = Resources.getResource(filepath);
		
		try {
			// Load from Resource
			String content = Resources.toString(url, Charsets.UTF_8);
			
			// Return Lines
			return content;
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
}

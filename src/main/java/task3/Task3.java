package task3;

import java.util.List;

public class Task3 {
	
	public Task3() {}
	
	public static int getOptimalValue(float money, List<Flear> flears){
		int res = 0,
			rating = 0,
			size = flears.size(),
			N = (int) Math.pow(2d, Double.valueOf(size));
		
		float tmpMoney = money;

		// Check all combinations
	    for (int i = 1; i < N; i++) {
	        String code = Integer.toBinaryString(N | i).substring(1);
	        
	        for (int j = 0; j < size; j++) {
	            if (code.charAt(j) == '1') {
	            	// Check if Money bigger then Flear Price
	            	if ((money - flears.get(j).getPrice()) >= 0) {
	            		// subtract Price from money
	            		money -= flears.get(j).getPrice();
	            		
	            		// Add Flear Rating to Rating
	            		rating += flears.get(j).getRating();
	            	}
	            }
	        }
	        
	        // If Flear Rating bigger then Max Rating, Set new Value
	        if (rating > res) res = rating;
	        
	        // Set Money and Rating to default
	        money = tmpMoney;
	        rating = 0;
	    }
	    
	    // Return optimal value
		return res;
	}
}

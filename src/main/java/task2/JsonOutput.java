package task2;

import java.util.ArrayList;

public class JsonOutput {
	public String mostExpensiveProduct;
	public String cheapestProduct;
	public String mostPopularProduct;
	
	public ArrayList<String> germanProducts;
	public ArrayList<String> chineseProducts;
	
	public boolean containsFragileProducts;
}

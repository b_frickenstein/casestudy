package task2;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import FileHandler.FileHandler;

public class Task2 {
	Collection<Products> productList;

	public Task2() {}

	public String exportJson() {
		// Create new Json Output
		JsonOutput output = new JsonOutput();

		// Set Data
		output.cheapestProduct = this.getCheapestProduct();
		output.containsFragileProducts = this.isFragileProducts();

		output.mostExpensiveProduct = this.getMostExpensiveProduct();
		output.mostPopularProduct = this.getMostPopularProduct();

		output.germanProducts = this.getProductsFrom("DE");
		output.chineseProducts = this.getProductsFrom("CN");

		// Return Json String
		return this.jsonBuild(output);
	}

	private String jsonBuild(JsonOutput output) {
		// Build "Pretty" Json String
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(output);
	}

	public void loadProducts() {
		// Load Products Data from file
		Gson gson = new Gson();
		Type collectionType = new TypeToken<Collection<Products>>() {}.getType();
		this.productList = gson.fromJson(FileHandler.getContent("task2/sampleProductsData.json"), collectionType);
	}

	public String getMostExpensiveProduct() {
		// Max Price found in List
		double maxPrice = 0;

		// Product name with Max Price
		String productName = "";

		for (Products product : this.productList) {
			// Check if Price bigger then MaxPrice
			if (product.price > maxPrice) {
				// Set Max Price and Product name
				maxPrice = product.price;
				productName = product.name;
			}
		}
		return productName;
	}

	public String getCheapestProduct() {
		// Cheapest Price
		double minPrice = 0;
		
		// Product name of cheapest Product
		String productName = "";

		for (Products product : this.productList) {
			// If min Price not set
			if (minPrice == 0) {
				// Set Price from first Product
				minPrice = product.price;
			} else {
				// If Product Price smaller then min Price
				if (product.price < minPrice) {
					// Set cheapest Price and Product name
					minPrice = product.price;
					productName = product.name;
				}
			}
		}
		return productName;
	}

	public String getMostPopularProduct() {
		// Max Purchased and Product name
		int timesPurchased = 0;
		String productName = "";

		for (Products product : this.productList) {
			// Check if Times Purchased bigger then saved Value
			if (product.timesPurchased > timesPurchased) {
				// Set new Value and Product name
				timesPurchased = product.timesPurchased;
				productName = product.name;
			}
		}
		return productName;
	}

	public ArrayList<String> getProductsFrom(String countryCode) {
		// Output Array List to Save Products from Country
		ArrayList<String> productsFrom = new ArrayList<>();

		for (Products product : this.productList) {
			// Check if County Match
			if (product.countryOfOrigin.equals(countryCode)) {
				// Add Product to Output
				productsFrom.add(product.name);
			}
		}
		return productsFrom;
	}

	public boolean isFragileProducts() {
		for (Products product : this.productList) {
			// Check if Product is Fragile
			if (product.isFragile) {
				// Fragile Product found
				return true;
			}
		}
		// No Match
		return false;
	}
}
